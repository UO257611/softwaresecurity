#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>

int target;

void winner()
{
	if (target != 5)
	{
		printf("Success!!\n");
	}
	exit(1);
}

void vuln()
{
	char buffer[64];
	target = 5;
	fgets(buffer, 64 ,stdin);
	printf("%s",buffer);

}

int main(int argc, char **argv)
{
	vuln();
}